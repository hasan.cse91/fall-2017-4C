package com.hellohasan.fall2017_4c;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText dueAmount;
    private EditText paidAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dueAmount = findViewById(R.id.dueAmount);
        paidAmount = findViewById(R.id.paidAmount);
    }

    public void calculateChangeAmount(View view) {
        double due = Double.parseDouble(dueAmount.getText().toString());
        double paid = Double.parseDouble(paidAmount.getText().toString());

        double change = paid - due;

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("change", change);
        startActivity(intent);

    }
}
