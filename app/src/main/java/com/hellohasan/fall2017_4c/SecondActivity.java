package com.hellohasan.fall2017_4c;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private TextView changeAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        changeAmount = findViewById(R.id.changeAmount);

        double change = getIntent().getDoubleExtra("change", 0);

        changeAmount.setText(String.valueOf("Change Amount: " + change));
    }
}
